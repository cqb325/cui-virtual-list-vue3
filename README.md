<p align="center">
    <a href="https://cqb325.gitee.io/cui-solid-doc">
        <img width="200" src="https://gitee.com/cqb325/cui-solid/raw/master/examples/assets/images/logo.svg">
    </a>
</p>

<h1>
CUI-Virtual-List-Vue3
    <h3>A simple vertical Virtual List based on vue3</h3>
</h1>

[![Virtual-list](https://img.shields.io/npm/v/cui-virtual-list-vue3.svg?style=flat-square)](https://www.npmjs.org/package/cui-virtual-list-vue3)
[![NPM downloads](https://img.shields.io/npm/dm/cui-virtual-list-vue3.svg?style=flat-square)](https://npmjs.org/package/cui-virtual-list-vue3)
[![NPM downloads](https://img.shields.io/npm/dt/cui-virtual-list-vue3.svg?style=flat-square)](https://npmjs.org/package/cui-virtual-list-vue3)
![JS gzip size](https://img.badgesize.io/https:/unpkg.com/cui-virtual-list-vue3/dist/cui-virtuallist-vue3.min.esm.js?label=gzip%20size%3A%20JS&compression=gzip&style=flat-square)


## Props
    
    // fixed height of container, choose between height/maxHeight
    height?: number
    // max height of container, choose between height/maxHeight
    maxHeight?: number
    // item estimated size, use to initialization content height
    itemEstimatedSize: number
    // prev or next number of items to show
    overscan?: number,
    // list data
    items: any[]

## Example

    export const createArray = (count: number) => {
        return new Array(count).fill(true).map(() => 1 + Math.round(Math.random() * 20))
    };

    export const ListItem = (props: any) : JSXElement => {
        const style = {...props.style}
        return <div
            style={style}
            role="listitem"
            classList={{
                "ListItemOdd": props.index % 2 === 0,
                "ListItemEven": props.index % 2 === 1,
            }}
            >
            <div> {new Array(props.item).fill(true).map(() => 'Row').join(" ")} Row {props.index}</div>
        </div>
    }

    ListItem.displayName = 'ListItem'

    <div style={{'250px', border: '1px solid #ccc'}}>
        <VirtualList height={250} items={createArray(10000)} itemEstimatedSize={20}>
            {ListItem}
        </VirtualList>
    </div>

## Install

    # npm
    npm install cui-virtual-list-vue3
    # yarn
    yarn add cui-virtual-list-vue3

