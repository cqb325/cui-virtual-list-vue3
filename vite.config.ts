import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import path from 'node:path';

export default defineConfig({
  plugins: [vue(), vueJsx()],
  build: {
    target: 'esnext',
    outDir: path.resolve(__dirname, './dist'),
    lib: {
        entry: path.resolve(__dirname, './src/index.tsx'),
        name: 'cui-virtuallist'
    },
    polyfillDynamicImport: false,
    rollupOptions: {
      context: 'globalThis',
      preserveEntrySignatures: 'strict',
      external: ['vue'],
      output: [
          {
              format: 'es',
              exports: 'named',
              sourcemap: false,
              entryFileNames: 'cui-virtuallist-vue3.min.esm.js',
              chunkFileNames: '[name].js',
              assetFileNames: '[name].[ext]',
              namespaceToStringTag: true,
              inlineDynamicImports: false,
              manualChunks: undefined,
          }
      ]
    }
  },
  resolve: {
    conditions: ['development', 'browser'],
    alias: {
    }
  },
  server: {
    port: 5200,
    host: '0.0.0.0',
  },
  base: './'
});
