export { default as VirtualListCore } from './core';
export declare function createUniqueId(): string;
export interface VirtualListProps {
    height?: number;
    maxHeight?: number;
    itemEstimatedSize: number;
    overscan?: number;
    items?: any[];
}
export interface MeasuredData {
    size: number;
    offset: number;
}
export interface IMeasuredDataMap {
    [key: number]: MeasuredData;
}
declare const _default: import("vue").DefineComponent<{
    height: {
        type: NumberConstructor;
        default: undefined;
    };
    maxHeight: {
        type: NumberConstructor;
        default: undefined;
    };
    itemEstimatedSize: {
        type: NumberConstructor;
        default: number;
        required: true;
    };
    overscan: {
        type: NumberConstructor;
        default: undefined;
    };
    items: {
        type: ArrayConstructor;
        default: undefined;
        required: true;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    height: {
        type: NumberConstructor;
        default: undefined;
    };
    maxHeight: {
        type: NumberConstructor;
        default: undefined;
    };
    itemEstimatedSize: {
        type: NumberConstructor;
        default: number;
        required: true;
    };
    overscan: {
        type: NumberConstructor;
        default: undefined;
    };
    items: {
        type: ArrayConstructor;
        default: undefined;
        required: true;
    };
}>>, {
    height: number;
    maxHeight: number;
    itemEstimatedSize: number;
    overscan: number;
    items: unknown[];
}, {}>;
export default _default;
