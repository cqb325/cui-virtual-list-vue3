export interface VirtualListCoreProps {
    height?: number;
    maxHeight?: number;
    itemEstimatedSize: number;
    overscan?: number;
    items: any[];
    scrollElement: any;
    contentElement: any;
    bodyElement: any;
}
export interface MeasuredData {
    size: number;
    offset: number;
}
export interface IMeasuredDataMap {
    [key: number]: MeasuredData;
}
declare const _default: import("vue").DefineComponent<{
    height: {
        type: NumberConstructor;
        default: undefined;
    };
    maxHeight: {
        type: NumberConstructor;
        default: undefined;
    };
    itemEstimatedSize: {
        type: NumberConstructor;
        default: number;
        required: true;
    };
    overscan: {
        type: NumberConstructor;
        default: undefined;
    };
    items: {
        type: ArrayConstructor;
        default: never[];
        required: true;
    };
    scrollElement: {
        type: FunctionConstructor;
        required: true;
    };
    contentElement: {
        type: FunctionConstructor;
        required: true;
    };
    bodyElement: {
        type: FunctionConstructor;
        required: true;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    height: {
        type: NumberConstructor;
        default: undefined;
    };
    maxHeight: {
        type: NumberConstructor;
        default: undefined;
    };
    itemEstimatedSize: {
        type: NumberConstructor;
        default: number;
        required: true;
    };
    overscan: {
        type: NumberConstructor;
        default: undefined;
    };
    items: {
        type: ArrayConstructor;
        default: never[];
        required: true;
    };
    scrollElement: {
        type: FunctionConstructor;
        required: true;
    };
    contentElement: {
        type: FunctionConstructor;
        required: true;
    };
    bodyElement: {
        type: FunctionConstructor;
        required: true;
    };
}>>, {
    height: number;
    maxHeight: number;
    itemEstimatedSize: number;
    overscan: number;
    items: unknown[];
}, {}>;
export default _default;
