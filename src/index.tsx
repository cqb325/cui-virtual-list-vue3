import VirtualListCore from "./core";
import { defineComponent, onMounted } from 'vue';

export {default as VirtualListCore} from './core';

let counter = 0;
const randomSeed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const index = Math.floor(Math.random() * randomSeed.length);
let id = randomSeed.substring(index, index + 1);
export function createUniqueId() {
    return `${id}_${counter++}`;
}

const CONTAINER_CLASSNAME = `cm-virtual-${createUniqueId()}`
let globalContainerStylesheet: HTMLStyleElement;

const insertGlobalStylesheet = () => {
    if (!globalContainerStylesheet) {
        globalContainerStylesheet = document.createElement('style')
        globalContainerStylesheet.type = 'text/css'

        globalContainerStylesheet.textContent = `
        .${CONTAINER_CLASSNAME} {
            position: relative !important;
            flex-shrink: 0 !important;
            width: 100%;
            height: 100%;
            overflow: auto;
        }
        .${CONTAINER_CLASSNAME} > * {
            width: 100%;
            will-change: transform !important;
            box-sizing: border-box !important;
            contain: strict !important;
            position: absolute !important;
            top: 0 !important;
            left: 0 !important;
        }
      `
        document.head.appendChild(globalContainerStylesheet)
    }
}

export interface VirtualListProps {
    height?: number,
    maxHeight?: number,
    itemEstimatedSize: number
    overscan?: number,
    items?: any[]
}

export interface MeasuredData {
    size: number,
    offset: number,
}
export interface IMeasuredDataMap {
    [key: number]: MeasuredData
}

export default defineComponent({
    name: 'VirtualList',
    props: {
        height: {type: Number, default: undefined},
        maxHeight: {type: Number, default: undefined},
        itemEstimatedSize: {type: Number, default: 20, required: true},
        overscan: {type: Number, default: undefined},
        items: {type: Array, default: undefined, required: true}
    },
    setup(props: VirtualListProps, {slots}) {
        insertGlobalStylesheet();

        const containerHeight = props.height ?? props.maxHeight;
        if (containerHeight === undefined) {
            console.error('Virtual List need height or maxHeight prop');
        }
    
        let scrollElement: any;
        let contentElement: any;
        let bodyElement: any;
        let core: any = null;

        const getScrollElement = () => scrollElement;
        const getContentElement = () => contentElement;
        const getBodyElement = () => bodyElement;

        onMounted(() => {
            core.init();
        })

        return () => <div class={CONTAINER_CLASSNAME} ref={(el) => scrollElement = el}>
            <div ref={(el) => contentElement = el}>
                <div ref={(el) => bodyElement = el}>
                    <VirtualListCore ref={(el) => core = el} scrollElement={getScrollElement} contentElement={getContentElement} bodyElement={getBodyElement}
                    {...props}>
                        {slots.default?.()}
                    </VirtualListCore>
                </div>
            </div>
        </div>
    },
})
