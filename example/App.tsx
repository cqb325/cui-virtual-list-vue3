import { defineComponent, ref, watchEffect } from "vue";
import VirtualList from '../src';

export const ListItem = (props: any) => {
    const style = {...props.style}
    return <div
      style={style}
      role="listitem"
      class={{
        "ListItemOdd": props.index % 2 === 0,
        "ListItemEven": props.index % 2 === 1,
      }}
    >
      <div>{new Array(props.item).fill(true).map(() => 'Row').join(" ")} Row {props.index}</div>
    </div>
}
ListItem.displayName = 'ListItem'

export const createArray = (count: number) => {
    return new Array(count).fill(true).map(() => 1 + Math.round(Math.random() * 20))
};

export default defineComponent({
    name: 'App',
    setup() {
        const data = ref<number[]>([]);
        const size = ref(250)
        const height = ref(250)
        data.value = createArray(100);
        return () => <div>
            <div style={{width: size.value + 'px', border: '1px solid #ccc'}}>
                <VirtualList height={height.value} items={data.value} itemEstimatedSize={20}>
                    <ListItem />
                </VirtualList>
            </div>
            <div style={{width: '250px', border: '1px solid #ccc'}}>
                <VirtualList maxHeight={250} items={data.value} itemEstimatedSize={20}>
                    <ListItem />
                </VirtualList>
            </div>
            <button onClick={() => {
                data.value = createArray(Math.round(Math.random() * 100) + 1)
            }}>修改数据</button>

            <button onClick={() => {
                data.value = createArray(2);
            }}>少量数据</button>

            <input type="range" min={250} max={500} onChange={(e) => {
                size.value = parseFloat(e.target?.value);
            }}/>
            <input type="range" min={250} max={500} onChange={(e) => {
                height.value = parseFloat(e.target?.value);
            }}/>
        </div>
    }
})